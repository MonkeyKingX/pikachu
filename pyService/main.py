from typing import Union
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

# 配置CORS 中间件
app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:4200"], # angular 应用地址
    allow_credentials=True,
    allow_methods=["GET", "POST"],
    allow_headers=["*"],
)

@app.get("/")
async def read_root():
    return {"Hello": "World"}

@app.get("/items/{item_id}")
async def read_item(item_id:int, q:Union[str, None]):
    return {"item_id": item_id, "q":q}
