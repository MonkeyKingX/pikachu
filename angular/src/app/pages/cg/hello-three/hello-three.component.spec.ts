import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HelloThreeComponent } from './hello-three.component';

describe('HelloThreeComponent', () => {
  let component: HelloThreeComponent;
  let fixture: ComponentFixture<HelloThreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HelloThreeComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(HelloThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
