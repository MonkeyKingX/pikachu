import { AfterViewChecked, AfterViewInit, CUSTOM_ELEMENTS_SCHEMA, Component, ElementRef, HostListener, OnInit,ViewChild } from '@angular/core';
import { NgtBeforeRenderEvent, NgtCanvas, extend } from 'angular-three';
import { Console } from 'node:console';
import * as THREE from 'three';
import { OrbitControls } from 'three-orbitcontrols-ts';

extend(THREE)


@Component({
  selector: 'app-hello-three',
  standalone: true,
  imports: [],
  templateUrl: './hello-three.component.html',
  styleUrl: './hello-three.component.css'
})
export class HelloThreeComponent implements OnInit, AfterViewInit{
  @ViewChild("container", {static: true})
  container: ElementRef<HTMLCanvasElement> | undefined;

  resizeObserver: ResizeObserver | undefined;
  scene : THREE.Scene = new THREE.Scene;
  camera : THREE.PerspectiveCamera = new THREE.PerspectiveCamera(75, 1/ 1, 0.1, 1000);
  renderer: THREE.WebGLRenderer | undefined ;
  constructor() {

  }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    this.initThreeJs();
  }

  @HostListener("window:resize", ['$event'])
  onResize(event: Event){
    const w = (this.container?.nativeElement.clientWidth || 100) -5;
    const h = (this.container?.nativeElement.clientHeight|| 100) -5;
    this.renderer?.setSize(w , h);
    this.camera.aspect = w / h;
    this.camera.updateProjectionMatrix();
  }

  initThreeJs(): void {
    this.renderer = new THREE.WebGLRenderer() ;// TODO:
    const w = (this.container?.nativeElement.clientWidth || 100) -5;
    const h = (this.container?.nativeElement.clientHeight|| 100) -5;
    this.renderer?.setSize(w , h);
    this.camera.aspect = w / h;
    this.camera.updateProjectionMatrix();
    this.container?.nativeElement.appendChild(this.renderer.domElement);

    const geometry = new THREE.BoxGeometry();
    const material = new THREE.MeshBasicMaterial({color: 0x00ff00});
    const cube = new THREE.Mesh(geometry, material);
    this.scene.add(cube);
    this.camera.position.z = 5;
    const animate = ()=> {
      requestAnimationFrame(animate);
      cube.rotation.x += 0.01;
      cube.rotation.y += 0.01;
      this.renderer?.render(this.scene, this.camera);
    }
    animate();
  }


}
