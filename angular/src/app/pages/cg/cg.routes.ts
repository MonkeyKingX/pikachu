import { Routes } from '@angular/router';
import { CGComponent } from './cg.component';

export const CG_ROUTES: Routes = [
  { path: '', component: CGComponent },
];
