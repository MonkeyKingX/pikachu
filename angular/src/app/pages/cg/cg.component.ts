import { Component } from '@angular/core';
import { NzContentComponent, NzFooterComponent, NzHeaderComponent, NzLayoutComponent, NzSiderComponent } from 'ng-zorro-antd/layout';
import { NzCardModule } from 'ng-zorro-antd/card';
import { HelloThreeComponent } from './hello-three/hello-three.component';
import { NgtCanvas } from 'angular-three';
import * as THREE from "three"

@Component({
  selector: 'app-cg',
  standalone: true,
  imports: [ NzLayoutComponent,
    NzHeaderComponent,
    NzFooterComponent,
    NzSiderComponent,
    NzContentComponent,
    NzCardModule,
    HelloThreeComponent,
    NgtCanvas],
  templateUrl: './cg.component.html',
  styleUrl: './cg.component.css'
})
export class CGComponent {
  readonly helloThree = HelloThreeComponent;
}
