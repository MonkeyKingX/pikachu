import { Routes } from '@angular/router';
import { DicomLabComponent } from './dicom-lab.component';


export const DCM_LAB_ROUTES: Routes = [
  { path: '', component: DicomLabComponent },
];
