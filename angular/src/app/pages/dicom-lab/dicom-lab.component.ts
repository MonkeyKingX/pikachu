import { Component } from '@angular/core';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzContentComponent, NzFooterComponent, NzHeaderComponent, NzLayoutComponent, NzSiderComponent } from 'ng-zorro-antd/layout';
import { NzButtonModule } from 'ng-zorro-antd/button'
import { NzButtonSize } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import axios from 'axios';


@Component({
  selector: 'app-dicom-lab',
  standalone: true,
  imports: [NzLayoutComponent,
    NzHeaderComponent,
    NzFooterComponent,
    NzContentComponent,
    NzSiderComponent,
    NzButtonModule,
    NzIconModule,
    NzGridModule],
  templateUrl: './dicom-lab.component.html',
  styleUrl: './dicom-lab.component.css'
})
export class DicomLabComponent {

    constructor(){
      this.getData();
    }

    getData(){
      axios.get('http://127.0.0.1:8081/', {
        headers: {
          "Content-Type": "application/json"
        }
      }).then(response=>{
        console.log(response.data);
      }).catch(error=>{
        console.error(error);
      })
    }

    openSelectFileDialog() {
      const inputElement: HTMLInputElement = document.createElement('input');
        inputElement.type = 'file';
        inputElement.click();

        inputElement.addEventListener('change', (event: any) => {
            const selectedFile = event.target.files[0];
            console.log("Selected file: ", selectedFile);
            // TODO: 在这里可以对选定的文件进行进一步处理
        });

    }
}
