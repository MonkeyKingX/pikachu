import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DicomLabComponent } from './dicom-lab.component';

describe('DicomLabComponent', () => {
  let component: DicomLabComponent;
  let fixture: ComponentFixture<DicomLabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DicomLabComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DicomLabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
