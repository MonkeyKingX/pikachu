import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MathToolsComponent } from './math-tools.component';

describe('MathToolsComponent', () => {
  let component: MathToolsComponent;
  let fixture: ComponentFixture<MathToolsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MathToolsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MathToolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
