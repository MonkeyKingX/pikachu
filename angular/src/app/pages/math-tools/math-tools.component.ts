import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NzFormControlComponent, NzFormItemComponent, NzFormModule } from 'ng-zorro-antd/form';
import { NzInputGroupComponent, NzInputModule } from 'ng-zorro-antd/input';
import { NzLayoutComponent } from 'ng-zorro-antd/layout';
import { NzInputNumberComponent, NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-math-tools',
  standalone: true,
  imports: [NzInputNumberComponent, NzInputGroupComponent, NzFormControlComponent],
  templateUrl: './math-tools.component.html',
  styleUrl: './math-tools.component.css',
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MathToolsComponent {
  toFixedValue = 2;
  cutValue = 2;
  customFnValue = 2;
  precision = 2;
  customPrecisionFn(value: string | number, precision?: number): number {
    return +Number(value).toFixed(precision! + 1);
  }
}
