import { Routes } from '@angular/router';
import { MathToolsComponent } from './math-tools.component';


export const MATH_TOOLS_ROUTES: Routes = [
  { path: '', component: MathToolsComponent },
];
