import { Routes } from '@angular/router';


export const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/welcome' },
  { path: 'welcome', loadChildren: () => import('./pages/welcome/welcome.routes').then(m => m.WELCOME_ROUTES) },
  { path: 'cg', loadChildren: ()=> import('./pages/cg/cg.routes').then(m=>m.CG_ROUTES)},
  { path: 'math-tools', loadChildren: () => import('./pages/math-tools/math-tools.routes').then(m => m.MATH_TOOLS_ROUTES)},
  { path: 'dicom-lab', loadChildren:()=> import('./pages/dicom-lab/dicom-lab.routes').then(m => m.DCM_LAB_ROUTES)}
];
